#!/bin/bash
# Generate a database of JSON data from ID3 tags extracted from MP3 files.
# This includes extracting the cover art and associating it with the JSON data.
# --------------------------------------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
DIR="."
lib="musiclib"
coverart="${lib}/coverart"

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 -d directory"
    echo "where"
    echo "-d directory  Set directory to scan."
    echo "              Default:  use current directory."
    echo ""
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":d:" Option
do
    case $Option in
    d) DIR=$OPTARG;;
    *) doHelp; exit 0;;
    esac
done

#--------------------------------------------------------------
# main
#--------------------------------------------------------------

# Change to the directory where we will genereate the database.
pushd "${DIR}"

echo "Processing $(pwd)."
read -p "ENTER to proceed or Ctrl-C to abort: "

# Create the database directory with it's coverart subdir.
mkdir -p "${coverart}"

# Now search the directory for MP3 files and process those that are found.
find . -iname "*.mp3" | while read file; do

    echo "Processing ${file}"

    # Get a UUID to uniquely identify the song.
    uuid=$(uuidgen)

    # Extract Cover Art, if any.
    ffmpeg -i "${file}" "${coverart}/${uuid}.png" 2>/dev/null

    # Extract ID3 data
    ffprobe -show_format -show_streams -print_format json "${file}" 2>/dev/null | jq .format.tags > ${lib}/${uuid}.json

    # Add the filename and covert art file to the extracted ID3 data.
    cat ${lib}/${uuid}.json | jq --arg file "${file}" '. + {file: $file}' > ${lib}/${uuid}.2
    mv ${lib}/${uuid}.2 ${lib}/${uuid}.json
    cat ${lib}/${uuid}.json | jq --arg coverart "${uuid}.png" '. + {coverart: $coverart}' > ${lib}/${uuid}.2
    mv ${lib}/${uuid}.2 ${lib}/${uuid}.json

done 

echo "Resizing all images to common size."
pushd ${coverart}
for file in $(ls -1); do
    convert "${file}" -resize 500x500\! "${file}"
done
popd

# Return to our original directory.
popd
echo "Database created in ${DIR}/${lib}"

# ls -l ${coverart}
# sleep 5
# cat ${lib}/*.json
