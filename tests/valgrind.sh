#!/bin/bash -p
# ------------------------------------------------------

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-gr] "
    echo "where"
    echo "-g    Generate suppressions"
    echo "-r    Enable show-reachable"
    echo "-v    "
    echo ""
    echo "Uses Valgrind to run the piboxd binary in test mode and check for memory leaks."
    echo "Should be run first, followed by unit tests."
    echo "Use Ctrl-C to stop the test and get leak reports from Valgrind."
    echo ""
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
GENSUP=
REACHABLE=
VERBOSE=
while getopts ":grv" Option
do
    case $Option in
    g)  GENSUP="--gen-suppressions=yes";;
    r)  REACHABLE="--show-reachable=yes";;
    v)  VERBOSE="-v";;
    *)  doHelp; exit 0;;
    esac
done

valgrind --tool=memcheck --leak-check=full ${VERBOSE} ${GENSUP} ${REACHABLE} --leak-resolution=high \
    --show-leak-kinds=all --num-callers=20 --suppressions=tests/gtk.suppression \
	src/musicfe -T -v3


