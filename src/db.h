/*******************************************************************************
 * musicfe
 *
 * db.h:  Functions for reading a MusicLib database.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef DB_H
#define DB_H

/* 
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

#define DB_VALID    1
#define DB_INVALID  2

/*
 * A movie entry
 */
typedef struct _music_t {
    char    *basedir;
    char    *uuid;
    char    *title;
    char    *album;
    char    *artist;
    char    *json;
    char    *file;
    gint    valid;
} MUSIC_T;

/*
 * ========================================================================
 * Defined values, some of which are used in test modes only
 * =======================================================================
 */

// Production locations for local media on USB sticks
#define DBTOP       "/media"

// Test location for local media
#define DBTOP_T     "data"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef DB_C
extern void dbLoad( void );
extern void clearCustomList ( void );
extern void playMusic( int idx );
extern void stopMusic( void );
extern void genList(char *searchTerm, int type);
extern gint getListSize (gint idx);
extern const char *getTrackTitle (gint idx);
extern int  getTrackNum (gint idx);
extern char *getPic (gint db, gint idx);
extern void dbDump();
#endif

#endif /* DB_H */
