/*******************************************************************************
 * musicfe
 *
 * player.h:  Start and stop musics.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PLAYER_H
#define PLAYER_H

/*========================================================================
 * Defined values
 *=======================================================================*/
#define OMX_FIFO        "/tmp/omx.f"

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PLAYER_C
extern void startPlayerProcessor( const char *, const char * );
extern void shutdownPlayerProcessor( void );
#endif /* !PLAYER_C */
#endif /* !PLAYER_H */
