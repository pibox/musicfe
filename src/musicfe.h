/*******************************************************************************
 * musicfe
 *
 * music.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MUSICFE_H
#define MUSICFE_H

#include <gtk/gtk.h>

#define PROG        "musicfe"

#define KEYSYMS_F     "/etc/pibox-keysyms"
#define KEYSYMS_FD    "data/pibox-keysyms"

/* DB types based on user selection */
#define DB_ALBUM    1
#define DB_ARTIST   2
#define DB_ALL      3

/*
 * Player choices
 */
#define DP_PLAY     1
#define DP_STOP     2

/*
 * Display Mode
 * 0: Carousal of Album or Artist
 * 1: Carousal of Album's for an artist
 * 2: List of tracks for an album
 */
#define DM_CAROUSEL     1
#define DM_ALBUMS       2
#define DM_TRACKS       3

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef MUSICFE_C
int     daemonEnabled = 0;
char    errBuf[256];
gchar   *imagePath = NULL;
gchar   *seriesImagePath = NULL;
gchar   *overview = NULL;
#else
extern int      daemonEnabled;
extern char     errBuf[];
extern gchar   *imagePath;
extern gchar   *seriesImagePath;
extern gchar   *overview;
#endif /* MUSICFE_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "musicfe"
#define MAXBUF      4096

/* Where the config file is located */
#define F_CFG           "/etc/musicfe.cfg"
#define F_CFG_T         "data/musicfe.cfg"
#define F_IMAGE_DIR     "/usr/share/musicfe"
#define F_IMAGE_DIR_T   "data"
#define F_DEFAULT_IMAGE "album.png"
#define F_DISPLAYCFG_T  "data/pibox-config"
#define F_DISPLAYCFG    "/etc/pibox-config"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef MUSICFE_C
extern void      musicTouch( int region );
extern void      do_drawing();
extern guint     getChoice( void );
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "player.h"
#include "db.h"
#include "cli.h"
#include "utils.h"

#endif /* !MUSICFE_H */

