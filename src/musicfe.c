/*******************************************************************************
 * musicfe
 *
 * musicfe.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MUSICFE_C

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>

#include "musicfe.h"

/* Local prototypes */
void do_menu_drawing();
void do_drawing();
static gboolean key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data);
void musicTouchGTK( int region );

GtkWidget       *window;
GtkWidget       *menuArea;
GtkWidget       *darea;
GtkWidget       *blankarea;
GtkWidget       *view;
GtkWidget       *notebook;
cairo_t         *cr = NULL;
cairo_t         *menucr = NULL;

// The home key is the key that exits the app.
guint homeKey = -1;

/*
 * DB Choice
 * 0: By Album
 * 1: By Artist
 */
guint db_choice = DB_ALBUM;

/*
 * Player choice (when in the track list)
 */
guint player_choice = DP_PLAY;

/*
 * Display Mode
 * DM_CAROUSEL: Carousel of Album or Artist
 * DM_ALBUMS: Carousel of Album's for an artist
 * DM_TRACKS: List of tracks for an album
 */
guint display_mode = DM_CAROUSEL;

/*
 * Current index for each db.
 */
guint album_idx = 0;
guint artist_idx = 0;
guint all_idx = 0;

/*
 * Track index.
 */
guint track_idx = 0;

/*
 * Current artist and album name.
 */
char *artist = NULL;
char *album  = NULL;

/* Default image dir */
char *defaultImagePath = NULL;

/* 
 * Font sizes depend on screen size.
 * Defaults to a normal sized screen.
 * Small screens reduce this size.
 */
gint largeFont  = 25;
gint mediumFont = 15;
gint smallFont  = 13;
gint summaryFont= 20;
gint listFont   = 20;

/*
 *========================================================================
 * Name:   musicTouch
 * Prototype:  void musicTouch( int region )
 *
 * Description:
 * Handler for touch region reports.  Basically it's a front end to the
 * key and button press handlers.
 *
 *   ------------------------------------
 *   | 0: N/A  | 1: N/A    | 2: Quit    | 
 *   ------------------------------------
 *   | 3: Back | 4: Select | 5: Forward |
 *   ------------------------------------
 *   | 6: N/A  | 7: N/A    | 8: N/A     |
 *   ------------------------------------
 *========================================================================
 */
void
musicTouch( int region )
{
    g_idle_add( (GSourceFunc)musicTouchGTK, GINT_TO_POINTER(region) );
}

void
musicTouchGTK( int region )
{
    static int          inprogress = 0;

    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        return;
    }
    inprogress = 1;

    switch( region )
    {
        case 0:
            break;

        case 1:
            break;

        case 2:
            piboxLogger(LOG_INFO, "Ctrl-Q key\n");
            gtk_main_quit();
            break;

        /* Left */
        case 3:
            piboxLogger(LOG_INFO, "Left 3\n");
            if ( db_choice == DB_ALBUM )
            {
                if ( album_idx > 0 )
                    album_idx--;
            }
            else
            {
                if ( artist_idx > 0 )
                    album_idx--;
            }
            do_drawing();
            break;

        /* Select */
        case 4:
            piboxLogger(LOG_INFO, "Select\n");
            playMusic( all_idx );
            break;

        /* Right */
        case 5:
            piboxLogger(LOG_INFO, "Right 5\n");
            if ( db_choice == DB_ALBUM )
            {
                if ( album_idx < getListSize(DB_ALBUM)-1 )
                    album_idx++;
            }
            else
            {
                if ( artist_idx < getListSize(DB_ARTIST)-1 )
                    album_idx++;
            }
            do_drawing();
            break;

        case 6:
            break;

        case 7:
            break;

        case 8:
            break;

        /* WTF? */
        default:
            piboxLogger(LOG_ERROR, "Invalid region: %d\n", region);
            break;
    }
    inprogress = 0;
}

/*
 *========================================================================
 * Name:   getChoice
 * Prototype:  guint getChoice( void )
 *
 * Description:
 * Retrieves the current DB choice: DB_ALBUM or DB_ARTIST
 *
 * Returns:
 * DB_ALBUM if user is browsing albums
 * DB_ARTIST if user is browsing artists
 *========================================================================
 */
guint
getChoice( void )
{
    return (db_choice);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    // Read in /etc/pibox-keysysm
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        // Grab second token
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        // Set the home key
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be 
 * handled.
 *
 * Notes:
 * Format is 
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig();
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();
    if ( (width<=800) || (height<=480) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
        largeFont  = 18;
        mediumFont = 12;
        smallFont  = 8;
        summaryFont= 15;
        listFont   = 10;
    }

    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        setCLIFlag(CLI_TOUCH);
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
    }
}

/*
 *========================================================================
 * Name:   do_carousel_drawing
 * Prototype:  void do_carousel_drawing( void )
 *
 * Description:
 * Updates the main display area.
 *========================================================================
 */
void
do_carousel_drawing()
{
    gfloat                  offset_x = 0.0;
    gfloat                  offset_y = 0.0;
    gfloat                  x_scaling = 0.0;
    GtkRequisition          req;
    int                     idx, i;

    char                    buf[16];
    char                    *imagePath;
    char                    *textField;
    static char             *title = NULL;
    char                    *pics[5] = { NULL, NULL, NULL, NULL, NULL};
    cairo_status_t          status;
    static cairo_surface_t  *image[5] = { NULL, NULL, NULL, NULL, NULL};
    static int              order[5] = { 0, 4, 1, 3, 2};
    int                     orderID;
    gfloat                  imageWidth[5] = { 0, 0, 0, 0, 0};
    gfloat                  imageHeight[5] = { 0, 0, 0, 0, 0};
    JSON_Value              *music_value;
    JSON_Object             *music_object;
    const char              *imageFilename;
    const char              *imageBasedir;
    const char              *field;
    PangoLayout             *layout;
    PangoRectangle          pangoRectangle;
    PangoFontDescription    *desc;
    struct stat             stat_buf;

    piboxLogger(LOG_INFO, "Entered\n");
    if ( cr != NULL )
        cairo_destroy(cr);
    piboxLogger(LOG_INFO, "Getting cr\n");
    cr = gdk_cairo_create(darea->window);
    req.width = gdk_window_get_width(darea->window);
    req.height = gdk_window_get_height(darea->window);

    cairo_set_source_rgb(cr, 0.1, 0.1, 0.1);
    cairo_set_line_width(cr, 1);
    cairo_rectangle(cr, 0, 0, req.width, req.height);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);

    /*
     * Get the 5 images to display.
     */
    if ( db_choice == DB_ALBUM )
    {
        piboxLogger(LOG_INFO, "Album list size: %d\n", getListSize(DB_ALBUM));
        idx = album_idx;
        textField = "album";
    }
    else if ( db_choice == DB_ARTIST )
    {
        piboxLogger(LOG_INFO, "Artist list size: %d\n", getListSize(DB_ARTIST));
        idx = artist_idx;
        textField = "artist";
    }
    else
    {
        piboxLogger(LOG_INFO, "Artist album list size: %d\n", getListSize(DB_ALL));
        idx = all_idx;
        textField = "artist";
    }


    /* Do this manually - it's easier to see what's happening. */
    i=0;
    pics[i++] = getPic(db_choice, idx-2);
    pics[i++] = getPic(db_choice, idx-1);
    pics[i++] = getPic(db_choice, idx);
    pics[i++] = getPic(db_choice, idx+1);
    pics[i++] = getPic(db_choice, idx+2);

    /* Grab and initialize the images we need. */
    for(i=0; i<5; i++)
    {
        /* Free up any previous images. */
        if ( image[i] != NULL )
        {
            cairo_surface_destroy(image[i]);
            image[i] = NULL;
        }

        if ( pics[i] != NULL )
        {
            /* Data is in JSON format, so parse it. */
            piboxLogger(LOG_INFO, "Image to display: %d %s\n", i, pics[i]);
            music_value = json_parse_string( (char *)pics[i] );
            music_object = json_value_get_object( music_value );

            /* Build the path to the image file */
            imageFilename = json_object_get_string( music_object, "coverart" );
            imageBasedir = json_object_get_string( music_object, "basedir" );
            if ( i==2 )
            {
                /* Get the artist and album names */
                field = json_object_get_string( music_object, "artist" );
                if ( field != NULL )
                {
                    if ( artist != NULL )
                        free(artist);
                    artist = strdup( field );
                    piboxLogger(LOG_INFO, "Selected Album: %s\n", album);
                }
                else
                    piboxLogger(LOG_INFO, "Selected Album: unknown\n");
                field = json_object_get_string( music_object, "album" );
                if ( field != NULL )
                {
                    if ( album != NULL )
                        free(album);
                    album = strdup( field );
                    piboxLogger(LOG_INFO, "Selected Artist: %s\n", artist);
                }
                else
                    piboxLogger(LOG_INFO, "Selected Artist: unknown\n");

                field = json_object_get_string( music_object, textField );
                title = strdup(field);
            }
            imagePath = g_malloc0(strlen(imageBasedir) + strlen(imageFilename) + 11);
            sprintf(imagePath, "%s/coverart/%s", imageBasedir, imageFilename);
            piboxLogger(LOG_INFO, "Image path: %s\n", imagePath);

            /* Verify the image exists.  If it doesn't we want to use the generic image. */
            if ( stat(imagePath, &stat_buf) != 0 )
            {
                free(imagePath);
                imagePath = g_malloc0(strlen(defaultImagePath) + 1);
                sprintf(imagePath, "%s", defaultImagePath);
                piboxLogger(LOG_INFO, "Using default image: %s\n", imagePath);
            }

            /* Import the image into Cairo */
            image[i] = cairo_image_surface_create_from_png(imagePath);
            status = cairo_surface_status(image[i]);
            if ( status != CAIRO_STATUS_SUCCESS )
            {
                piboxLogger(LOG_ERROR, "Failure loading image: %s\n", 
                        cairo_status_to_string(status));
            }

            /* Cleanup */
            free(imagePath);
            json_value_free(music_value);

            /* Grab the size information for the image */
            imageWidth[i] = cairo_image_surface_get_width(image[i]);
            imageHeight[i] = cairo_image_surface_get_height(image[i]);
            piboxLogger(LOG_INFO, "image %d w/h: %f / %f\n", i, imageWidth[i], imageHeight[i]);

        }
        else
        {
            /* This entry doesn't have an image to display */
            piboxLogger(LOG_INFO, "Image to display: %d none\n", i);
            image[i] = NULL;
        }
    }

    /* 
     * We need to draw the image back to front, not in ascending order.
     * This is so that the larger images overlay the images to either side.
     */
    for(i=0; i<5; i++)
    {
        /* Which one do we want to process? */
        orderID = order[i];

        /* Scaling factors */
        switch (orderID) {
            // Leftmost, smallest
            case 0: 
                if ( image[0] != NULL )
                {
                    x_scaling = .25;
                    offset_x = -1.0 * ( (imageWidth[2] * .7 * .5)  + 
                                        (imageWidth[1] * .5 ) + 
                                        (imageWidth[0] * .25 ) - 40);
                    offset_y = -1.0 * (imageHeight[0] * x_scaling * .5);
                }
                break;

            // Next smallest, left side
            case 1: 
                if ( image[1] != NULL )
                {
                    x_scaling = .5;
                    offset_x = -1.0 * ((imageWidth[2] * .7 * .5) + (imageWidth[1] * .5) - 20);
                    offset_y = -1.0 * (imageHeight[1] * x_scaling * .5);
                }
                break;

            // Full size
            case 2: 
                if ( image[2] != NULL )
                {
                    x_scaling = .7;
                    offset_x = -1.0 * (imageWidth[2] * x_scaling * .5);
                    offset_y = -1.0 * (imageHeight[2] * x_scaling * .5);
                }
                break;

            // Next smallest, right side
            case 3: 
                if ( image[3] != NULL )
                {
                    x_scaling = .5;
                    offset_x = (imageWidth[2] * .7 * .5) - 20;
                    offset_y = -1.0 * (imageHeight[3] * x_scaling * .5);
                }
                break;

            // Rightmost, smallest
            case 4: 
                if ( image[4] != NULL )
                {
                    x_scaling = .25;
                    offset_x = ((imageWidth[2] * .7 * .5) - 20) + ((imageWidth[3] * .5 ) - 20);
                    offset_y = -1.0 * (imageHeight[4] * x_scaling * .5);
                }
                break;
        }


        if ( image[orderID] != NULL )
        {
            piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);
            piboxLogger(LOG_INFO, "Drawing image %d\n", orderID);
            piboxLogger(LOG_INFO, "%d: offset_x/_y: %f/%f - scaling : %f\n", 
                    orderID, offset_x, offset_y, x_scaling);

            /* Position for drawing */
            cairo_translate(cr, 
                    (req.width/2) + offset_x,
                    (req.height/2) + offset_y);

            /* Scale to fit */
            cairo_scale (cr, x_scaling, x_scaling);
            cairo_set_source_surface (cr, image[orderID], 0, 0);

            /* Reset scaling */
            x_scaling = 1/x_scaling;
            cairo_scale (cr, x_scaling, x_scaling);

            /* Recenter */
            cairo_translate(cr, 
                    -1.0 * ((req.width/2) + offset_x),
                    -1.0 * ((req.height/2) + offset_y));

            cairo_paint(cr);
        }
    }

    /*
     * Generate text that is wrapped in the gray box.
     */
    if ( title != NULL )
    {
        layout = pango_cairo_create_layout(cr);

        sprintf(buf, "Sans Bold %d", summaryFont);
        desc = pango_font_description_from_string(buf);
        pango_font_description_set_absolute_size(desc, summaryFont*PANGO_SCALE);
        pango_layout_set_font_description(layout, desc);
        pango_font_description_free(desc);

        pango_layout_set_wrap(layout, PANGO_WRAP_WORD);
        pango_layout_set_width(layout, ((int)(req.width)-20)*PANGO_SCALE);

        pango_layout_set_text(layout, title, -1);
        pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
        piboxLogger(LOG_INFO, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);

        cairo_set_source_rgb(cr, 0.9, 0.9, 1.0);
        pango_cairo_update_layout(cr, layout);

        cairo_translate(cr, 
                (req.width/2)-(pangoRectangle.width/2), 
                (req.height/2 + (imageHeight[2]*.7)/2 + 20) );
        pango_cairo_show_layout(cr, layout);

        g_object_unref(layout);
    }
}

/*
 *========================================================================
 * Name:   do_tracks_drawing
 * Prototype:  void do_tracks_drawing( void )
 *
 * Description:
 * Updates the main display area.
 *========================================================================
 */
void
do_tracks_drawing()
{
    GtkRequisition          req;
    int                     idx, listSize, heightOffset=10, trackNum;
    PangoLayout             *layout;
    char                    buf[256];
    PangoFontDescription    *desc;
    PangoRectangle          pangoRectangle;
    const char              *title = NULL;
    PangoAttrList           *attrs = NULL;

    piboxLogger(LOG_INFO, "Entered\n");
    if ( cr != NULL )
        cairo_destroy(cr);
    piboxLogger(LOG_INFO, "Getting cr\n");
    cr = gdk_cairo_create(darea->window);
    req.width = gdk_window_get_width(darea->window);
    req.height = gdk_window_get_height(darea->window);

    cairo_set_source_rgb(cr, 0.1, 0.1, 0.1);
    cairo_set_line_width(cr, 1);
    cairo_rectangle(cr, 0, 0, req.width, req.height);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);

    listSize = getListSize(DB_ALL);
    piboxLogger(LOG_INFO, "Custom list size: %d\n", listSize);

    layout = pango_cairo_create_layout(cr);
    sprintf(buf, "Monospace Bold %d", summaryFont);
    desc = pango_font_description_from_string(buf);
    pango_font_description_set_absolute_size(desc, summaryFont*PANGO_SCALE);
    pango_layout_set_font_description(layout, desc);
    pango_font_description_free(desc);
    pango_layout_set_wrap(layout, PANGO_WRAP_WORD);
    pango_layout_set_width(layout, ((int)(req.width)-20)*PANGO_SCALE);
    pango_layout_set_spacing(layout, 1);
    pango_layout_set_single_paragraph_mode(layout, TRUE);

    /* Initial translation. */
    cairo_translate(cr, 10, 0);

    for(idx=0; idx<listSize; idx++)
    {
        title = getTrackTitle(idx);
        if ( title == NULL )
            continue;
        trackNum = getTrackNum(idx);

        if ( trackNum == -1 )
            sprintf(buf, "-- %s", title);
        else
            sprintf(buf, "%2d %s", trackNum, title);
        piboxLogger(LOG_INFO, "Title: %s\n", title);

        /* Underline the active entry */
        if ( idx == track_idx )
        {
            attrs = pango_attr_list_new();
            pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_SINGLE));
            pango_layout_set_attributes (layout, attrs);
        }
        else
        {
            attrs = pango_attr_list_new();
            pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_NONE));
            pango_layout_set_attributes (layout, attrs);
        }

        pango_layout_set_text(layout, buf, -1);
        pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
        piboxLogger(LOG_INFO, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);

        cairo_set_source_rgb(cr, 0.9, 0.9, 1.0);
        pango_cairo_update_layout(cr, layout);

        cairo_translate(cr, 0, heightOffset);
        pango_cairo_show_layout(cr, layout);

        heightOffset = pangoRectangle.height + 2;

        /* Cleanup */
        pango_attr_list_unref(attrs);
    }

    g_object_unref(layout);
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( void )
 *
 * Description:
 * Updates the poster area of the display.
 *========================================================================
 */
void
do_drawing()
{
    switch ( display_mode )
    {
        /* Carousel of either albums or artists */
        case DM_CAROUSEL:
            do_carousel_drawing();
            break;

        /* Carousel of albums for a specific artist */
        case DM_ALBUMS:
            do_carousel_drawing();
            break;

        /* List of track for a specific albums */
        case DM_TRACKS:
            piboxLogger(LOG_INFO, "Calling do_tracks_drawing()\n");
            do_tracks_drawing();
            break;
    }
}

/*
 *========================================================================
 * Name:   do_player_control_drawing
 * Prototype:  void do_player_control_drawing( void )
 *
 * Description:
 * Updates the player control area of the display.
 *========================================================================
 */
void
do_player_control_drawing()
{
    GtkRequisition req;

    menucr = gdk_cairo_create(menuArea->window);
    req.width = gdk_window_get_width(menuArea->window);
    req.height = gdk_window_get_height(menuArea->window);
    piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);

    // Fill with black background
    cairo_set_source_rgb(menucr, 0.0, 0.0, 0.0);
    cairo_set_line_width(menucr, 1);
    cairo_rectangle(menucr, 0, 0, req.width, req.height);
    cairo_stroke_preserve(menucr);
    cairo_fill(menucr);

    cairo_select_font_face(menucr, "Purisa",
        CAIRO_FONT_SLANT_NORMAL,
        CAIRO_FONT_WEIGHT_BOLD);

    if ( player_choice == DP_PLAY )
    {
        cairo_set_font_size(menucr, mediumFont);
        cairo_set_source_rgb(menucr, 1.0, 1.0, 1.0);
    }
    else
    {
        cairo_set_font_size(menucr, smallFont);
        cairo_set_source_rgb(menucr, 0.75, 0.75, 0.75);
    }
    cairo_move_to(menucr, 20, 25);
    cairo_show_text(menucr, "Play");

    if ( player_choice == DP_STOP )
    {
        cairo_set_font_size(menucr, mediumFont);
        cairo_set_source_rgb(menucr, 1.0, 1.0, 1.0);
    }
    else
    {
        cairo_set_font_size(menucr, smallFont);
        cairo_set_source_rgb(menucr, 0.75, 0.75, 0.75);
    }
    cairo_move_to(menucr, 220, 25);
    cairo_show_text(menucr, "Stop");
}

/*
 *========================================================================
 * Name:   do_menu_drawing
 * Prototype:  void do_menu_drawing( void )
 *
 * Description:
 * Updates the menu area of the display.
 *
 * Notes:
 * See http://zetcode.com/gfx/cairo/cairotext/
 *========================================================================
 */
void
do_menu_drawing()
{
    GtkRequisition req;

    menucr = gdk_cairo_create(menuArea->window);
    req.width = gdk_window_get_width(menuArea->window);
    req.height = gdk_window_get_height(menuArea->window);
    piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);

    // Fill with black background
    cairo_set_source_rgb(menucr, 0.0, 0.0, 0.0);
    cairo_set_line_width(menucr, 1);
    cairo_rectangle(menucr, 0, 0, req.width, req.height);
    cairo_stroke_preserve(menucr);
    cairo_fill(menucr);

    cairo_select_font_face(menucr, "Purisa",
        CAIRO_FONT_SLANT_NORMAL,
        CAIRO_FONT_WEIGHT_BOLD);

    if ( db_choice == DB_ALBUM )
    {
        cairo_set_font_size(menucr, mediumFont);
        cairo_set_source_rgb(menucr, 1.0, 1.0, 1.0);
    }
    else
    {
        cairo_set_font_size(menucr, smallFont);
        cairo_set_source_rgb(menucr, 0.75, 0.75, 0.75);
    }
    cairo_move_to(menucr, 20, 25);
    cairo_show_text(menucr, "By Album");

    if ( db_choice == DB_ARTIST )
    {
        cairo_set_font_size(menucr, mediumFont);
        cairo_set_source_rgb(menucr, 1.0, 1.0, 1.0);
    }
    else
    {
        cairo_set_font_size(menucr, smallFont);
        cairo_set_source_rgb(menucr, 0.75, 0.75, 0.75);
    }
    cairo_move_to(menucr, 220, 25);
    cairo_show_text(menucr, "By Artist");
}

/*
 *========================================================================
 * Name:   on_draw_event
 * Prototype:  void on_draw_event( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the poster area of the display when realize and expose events occur.
 *========================================================================
 */
static gboolean
on_draw_event(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    do_drawing();
    return(TRUE);
}

/*
 *========================================================================
 * Name:   on_menudraw_event
 * Prototype:  void on_menudraw_event( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the menu area of the display.
 *========================================================================
 */
static gboolean
on_menudraw_event(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    do_menu_drawing();
    return(TRUE);
}

#if 0
/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a double click in the poster to start a music.
 *========================================================================
 */
static gboolean
button_press(GtkWidget *widget, GdkEventButton *event)
{
    switch(event->type) {
        case GDK_2BUTTON_PRESS:
            // g_print("Mouse button %d double-click at coordinates (%lf,%lf)\n", event->button,event->x,event->y);
            playMusic();
            break;
        default:
            break;
    }
    return(TRUE);
}
#endif

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    static int processing=0;

    /* Lock out keyboard bounce until we're done. */
    if ( processing != 0 )
        return(FALSE);
    processing=1;

    switch(event->keyval)
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                gtk_main_quit();
            }
            break;

        case GDK_KEY_Left:
        case GDK_KEY_KP_Left:
            if ( db_choice == DB_ALBUM )
            {
                if ( album_idx > 0 )
                    album_idx--;
            }
            else if ( db_choice == DB_ARTIST )
            {
                if ( artist_idx > 0 )
                    artist_idx--;
            }
            else
            {
                if ( all_idx > 0 )
                    all_idx--;
            }
            do_drawing();
            break;

        case GDK_KEY_Right:
        case GDK_KEY_KP_Right:
            if ( db_choice == DB_ALBUM )
            {
                if ( album_idx < getListSize(DB_ALBUM)-1 )
                    album_idx++;
            }
            else if ( db_choice == DB_ARTIST )
            {
                if ( artist_idx < getListSize(DB_ARTIST)-1 )
                    artist_idx++;
            }
            else
            {
                if ( all_idx < getListSize(DB_ALL)-1 )
                    all_idx++;
            }
            do_drawing();
            break;

        case GDK_KEY_Down:
        case GDK_KEY_KP_Down:
            if ( display_mode == DM_TRACKS )
            {
                if ( track_idx < getListSize(DB_ALL)-1 )
                    track_idx++;
            }
            do_drawing();
            break;

        case GDK_KEY_Up:
        case GDK_KEY_KP_Up:
            if ( display_mode == DM_TRACKS )
            {
                if ( track_idx > 0)
                    track_idx--;
            }
            do_drawing();
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key: db_choice=%d\n", db_choice);
            if ( (db_choice == DB_ARTIST) || (db_choice == DB_ALBUM) )
            {
                gtk_main_quit();
            }
            else 
            {
                clearCustomList();
                display_mode = DM_CAROUSEL;
                db_choice = DB_ALBUM;
            }
            do_menu_drawing();
            do_drawing();
            break;

        case GDK_KEY_Tab:
            piboxLogger(LOG_INFO, "Tab key\n");
            if ( display_mode != DM_TRACKS )
            {
                switch ( db_choice )
                {
                    case DB_ALBUM:  
                        db_choice = DB_ARTIST;
                        do_menu_drawing();
                        do_drawing();
                        break;
                    case DB_ARTIST: 
                        db_choice = DB_ALBUM;
                        do_menu_drawing();
                        do_drawing();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                /* Move between player controls */
                switch ( player_choice )
                {
                    case DP_PLAY:  
                        player_choice = DP_STOP;
                        do_player_control_drawing();
                        break;
                    case DP_STOP:  
                        player_choice = DP_PLAY;
                        do_player_control_drawing();
                        break;
                }
            }
            break;

        case GDK_KEY_Return:
        case GDK_KEY_KP_Enter:
        case GDK_KEY_space:
        case GDK_KEY_KP_Space:
            piboxLogger(LOG_INFO, "Select key\n");
            switch ( display_mode )
            {
                case DM_CAROUSEL:
                    if ( db_choice == DB_ARTIST )
                    {
                        if ( artist != NULL )
                        {
                            all_idx=0;
                            genList((char *)artist, DB_ARTIST);
                            display_mode = DM_ALBUMS;
                            db_choice = DB_ALL;
                        }
                    }
                    else if ( db_choice == DB_ALBUM )
                    {
                        if ( album != NULL )
                        {
                            all_idx=0;
                            piboxLogger(LOG_INFO, "Album: %s\n", album);
                            genList((char *)album, DB_ALBUM);
                            display_mode = DM_TRACKS;
                            db_choice = DB_ALL;

                            /* Reset player controls */
                            player_choice = DP_PLAY;
                            do_player_control_drawing();
                        }
                    }
                    break;

                case DM_ALBUMS:
                    all_idx=0;
                    genList((char *)album, DB_ALBUM);
                    display_mode = DM_TRACKS;
                    db_choice = DB_ALL;

                    /* Reset player controls */
                    player_choice = DP_PLAY;
                    do_player_control_drawing();
                    break;

                case DM_TRACKS:
                    if ( player_choice == DP_PLAY )
                    {
                        /* Pass the current index to the player front end. */
                        playMusic( track_idx );
                    }
                    else
                        stopMusic();
                    break;
            }
            do_drawing();
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                if ( (db_choice == DB_ARTIST) || (db_choice == DB_ALBUM) )
                {
                    gtk_main_quit();
                }
                else 
                {
                    clearCustomList();
                    display_mode = DM_CAROUSEL;
                    db_choice = DB_ALBUM;
                }
                do_menu_drawing();
                do_drawing();
            }
            else
            {
                piboxLogger(LOG_INFO, "Ignoring unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(TRUE);
            }
            break;
    }
    processing = 0;
    return(TRUE);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates a black top level window.  This is needed in kiosk mode to avoid
 * the flashing that comes with changing musics.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *vbox;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    // Two rows: menu, CD covers
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show (vbox);
    gtk_container_add (GTK_CONTAINER (window), vbox);

    /* Menu area */
    menuArea = gtk_drawing_area_new();
    gtk_widget_set_size_request( menuArea, 800, 40);
    gtk_box_pack_start (GTK_BOX (vbox), menuArea, FALSE, FALSE, 0);

    gtk_widget_add_events(menuArea, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(G_OBJECT(menuArea), "expose_event", G_CALLBACK(on_menudraw_event), NULL);

    /*
     * The CD covers area
     */
    darea = gtk_drawing_area_new();
    gtk_widget_set_size_request( darea, piboxGetDisplayWidth(), piboxGetDisplayHeight());
    g_signal_connect(G_OBJECT(darea), "expose_event", G_CALLBACK(on_draw_event), NULL);
    gtk_box_pack_start (GTK_BOX (vbox), darea, TRUE, TRUE, 0);

    /*
     * The List for album tracks.
     */

    // ------------------------------------------------------------------------
    // Make the main window die when destroy is issued.
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    // Now position the window and set its title
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), piboxGetDisplayWidth(), piboxGetDisplayHeight());
    gtk_window_set_title(GTK_WINDOW(window), "MusicFE Kiosk");

    return window;
}

#if 0
/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Reset the application. */
            // TBD
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            daemonEnabled = 0;
            break;
    }
}
#endif

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    char    cwd[512];
    char    gtkrc[1024];

    GtkWidget *window;
    struct stat stat_buf;
    char path[MAXBUF];

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }
    piboxLogger(LOG_INFO, "vtsrc: %s\n", cliOptions.vt);
    piboxLogger(LOG_INFO, "vttmp: %s\n", cliOptions.vttmp);

    // Read environment config for keyboard behaviour
    loadKeysyms();

    /* Get display config information */
    loadDisplayConfig();
    piboxLogger(LOG_INFO, "display type: %s\n", cliOptions.display_type);
    piboxLogger(LOG_INFO, "display resolution: %s\n", cliOptions.display_resolution);

    // Set the path to the top of the music libraries.
    memset(path, 0, MAXBUF);
    if ( cliOptions.dbTop == NULL )
    {
        piboxLogger(LOG_INFO, "dbTop is null\n");
        if ( isCLIFlagSet( CLI_TEST) )
            sprintf(path, "%s", DBTOP_T);
        else
            sprintf(path, "%s", DBTOP);
    }
    else
    {
        sprintf(path, "%s", cliOptions.dbTop);
        piboxLogger(LOG_INFO, "dbTop is %s\n", path);
    }

    // Test for db existance.
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find database directory");
        return(-1);
    }
    piboxLogger(LOG_INFO, "Reading database directory: %s\n", path);

    // Get the current working directory.
    memset(cwd, 0, 512);
    getcwd(cwd, 512);

    /* Set the image_dir */
    if ( isCLIFlagSet( CLI_TEST) )
    {
        defaultImagePath = 
                g_malloc0(strlen(cwd) + strlen(F_IMAGE_DIR_T) + strlen(F_DEFAULT_IMAGE) + 3);
        sprintf(defaultImagePath, "%s/%s/%s", cwd, F_IMAGE_DIR_T, F_DEFAULT_IMAGE);
    }
    else
    {
        defaultImagePath = g_malloc0(strlen(F_IMAGE_DIR) + strlen(F_DEFAULT_IMAGE) + 2);
        sprintf(defaultImagePath, "%s/%s", F_IMAGE_DIR, F_DEFAULT_IMAGE);
    }

    // Change to the DB directory
    sprintf(gtkrc, "%s/data/gtkrc", cwd);
    piboxLogger(LOG_INFO, "gtkrc: %s\n", gtkrc);
    piboxLogger(LOG_INFO, "Changing to data directory: %s\n", path);
    chdir(path);

    piboxLogger(LOG_INFO, "Running from: %s\n", getcwd(path, MAXBUF));

    /* 
     * If we're on a touchscreen, launch the input handler 
     * Register a handler for us.
     */
    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        piboxLogger(LOG_INFO, "Registering musicTouch.\n");
        piboxTouchRegisterCB(musicTouch, TOUCH_REGION);
        piboxTouchStartProcessor();
    }

    gtk_init(&argc, &argv);

    // Load db
    dbLoad();
    // dbDump();

    if ( isCLIFlagSet( CLI_TEST) )
        gtk_rc_parse(gtkrc);
    else
        piboxLogger(LOG_INFO, "CLI_TEST is not set.\n");
    window = createWindow();
    gtk_window_fullscreen (GTK_WINDOW(window));
    gtk_widget_show_all(window);

    // Set keyboard focus.
    gtk_widget_grab_default(view);
    gtk_widget_grab_focus(view);

    gtk_main();
    shutdownPlayerProcessor();
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        /* SIGINT is required to exit the touch processor thread. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }

    piboxLoggerShutdown();
    return 0;
}
