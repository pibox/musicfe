/*******************************************************************************
 * musicfe
 *
 * db.c:  Functions for reading a MusicLib database.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>
#include <pibox/utils.h>
#include <pibox/parson.h>

#include "musicfe.h"

/*
 * Linked lists for db files.
 */
GSList  *dirList = NULL;        // List of directories for music
GSList  *musicList = NULL;      // List of music

GSList  *albumList = NULL;      // List of music by album
GSList  *artistList = NULL;     // List of music by artist
GSList  *allList = NULL;        // The full database.
GSList  *customList = NULL;     // List of albums by specific artist or tracks by album

GSList  *activeMusic = NULL;

static char readbuf[PATH_MAX];

static JSON_Value *root_value = NULL;
static JSON_Object *root_object = NULL;
static JSON_Value *music_value = NULL;
static JSON_Array *music_array = NULL;

/*
 *========================================================================
 * Name:   getDirs
 * Prototype:  void getDirs( void )
 *
 * Description:
 * Generate a list of music db directories.
 *========================================================================
 */
void 
getDirs ( GSList **list )
{
    FILE    *pd = NULL;
    char    findCmd[128];
    char    *pathPtr;
    char    *dir;

    if ( isCLIFlagSet( CLI_TEST) )
    {
        sprintf(findCmd, "find -L . -type d -name musiclib");
    }
    else
    {
        sprintf(findCmd, "find . -type d -name musiclib");
    }
    piboxLogger(LOG_INFO, "Find command: %s\n", findCmd);

    // Find all databases in top level path.
    pd = popen(findCmd, "r");
    if ( pd == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find database directories");
        return;
    }

    // Read each line
    while( fgets(readbuf, PATH_MAX, pd) != NULL )
    {
        pathPtr = readbuf;
        pathPtr = piboxTrim(pathPtr);
        piboxStripNewline(pathPtr);
        dir = strdup(pathPtr);
        piboxLogger(LOG_INFO, "Music dir: %s\n", dir);
        *list = g_slist_append(*list, dir);
    }

    pclose(pd);
}

/*
 *========================================================================
 * Name:   albumExists
 * Prototype:  gint albumExists( gconstpointer item_data, gconstpointer user_data )
 *
 * Description:
 * Search for a given entry in the album list.
 *========================================================================
 */
gint 
albumExists ( gconstpointer item_data, gconstpointer user_data )
{
    JSON_Value  *music_a_value;
    JSON_Object *music_a_object;
    const char  *album_a;
    int         rc;

    music_a_value  = json_parse_string( (char *)item_data );
    music_a_object = json_value_get_object( music_a_value );
    album_a = json_object_get_string( music_a_object, "album" );

    rc = strcasecmp( album_a, (char *)user_data );
    json_value_free(music_a_value);

    return(rc);
}

/*
 *========================================================================
 * Name:   trackExists
 * Prototype:  gint trackExists( gconstpointer item_data, gconstpointer user_data )
 *
 * Description:
 * Search for a given track in the custom list.
 *========================================================================
 */
gint 
trackExists ( gconstpointer item_data, gconstpointer user_data )
{
    JSON_Value  *music_a_value;
    JSON_Object *music_a_object;
    const char  *track;
    int         rc;

    music_a_value  = json_parse_string( (char *)item_data );
    music_a_object = json_value_get_object( music_a_value );
    track = json_object_get_string( music_a_object, "track" );

    piboxLogger(LOG_INFO, "want: %s  --  have: %s\n", track, (char *)user_data);
    rc = strcasecmp( track, (char *)user_data );
    json_value_free(music_a_value);

    return(rc);
}

/*
 *========================================================================
 * Name:   artistExists
 * Prototype:  gint artistExists( gconstpointer item_data, gconstpointer user_data )
 *
 * Description:
 * Search for a given entry in the artist list.
 *========================================================================
 */
gint 
artistExists ( gconstpointer item_data, gconstpointer user_data )
{
    JSON_Value  *music_a_value;
    JSON_Object *music_a_object;
    const char  *artist_a;
    int         rc;

    music_a_value  = json_parse_string( (char *)item_data );
    music_a_object = json_value_get_object( music_a_value );
    artist_a = json_object_get_string( music_a_object, "artist" );

    rc = strcasecmp( artist_a, (char *)user_data );
    json_value_free(music_a_value);

    return(rc);
}

/*
 *========================================================================
 * Name:   buildArtistDB
 * Prototype:  void buildArtistDB( gconstpointer item, gconstpointer user_data )
 *
 * Description:
 * Build a DB for the specified artist
 *========================================================================
 */
void 
buildArtistDB ( gconstpointer item, gconstpointer user_data )
{
}

/*
 *========================================================================
 * Name:   readDB
 * Prototype:  void readDB( void )
 *
 * Description:
 * Read JSON data from the specified directory.
 *========================================================================
 */
void 
readDB ( gconstpointer item, gconstpointer user_data )
{
    DIR             *dir;
    struct dirent   *ent;
    char            *path = (char *)item;
    char            *basedir;
    char            *idx;
    char            *uuid;
    char            *offset;
    char            *album;
    char            *artist;
    char            *all;
    const char      *field;
    char            filename[PATH_MAX];
    struct stat     stat_buf;
    JSON_Value      *entry_value;
    JSON_Object     *entry_object;

    piboxLogger(LOG_INFO, "Directory to scan: %s\n", path);
    basedir = strdup(path);
    idx = strstr(basedir, "/musiclib");
    *idx = '\0';

    if ( (dir = opendir (path)) != NULL) 
    {
        while ((ent = readdir (dir)) != NULL) 
        {
            if ( strncmp(ent->d_name, ".", 1) == 0 )
                continue;
            if ( strncmp(ent->d_name, "coverart", 1) == 0 )
                continue;

            /* Read the file into a struct. */
            sprintf(filename, "%s/%s", path, ent->d_name);
            piboxLogger(LOG_INFO, "json file: %s\n", filename);

            if ( stat(filename, &stat_buf) != 0 )
            {
                piboxLogger(LOG_ERROR, "Error stat of file: %s, reason = %d\n", filename, errno);
                continue;
            }
            if ( stat_buf.st_size > 0 )
            {
                uuid = strdup(ent->d_name);
                offset = index(uuid, '.');
                if ( offset != NULL )
                    *offset = '\0';

                entry_value = json_parse_file( filename );
                entry_object = json_value_get_object( entry_value );
                json_object_set_string( entry_object, "basedir", path );
                json_object_set_string( entry_object, "uuid", uuid);

                /* Convert back to JSON string */
                all = json_serialize_to_string(entry_value);

                /* Add to GSList - makes it easier to sort later */
                allList = g_slist_append(allList, all);

                field = json_object_get_string( entry_object, "album" );
                if ( field != NULL )
                {
                    if ( (albumList == NULL ) ||
                       (g_slist_find_custom( albumList, field, albumExists) == NULL ) )
                    {
                        album = json_serialize_to_string(entry_value);
                        albumList = g_slist_append(albumList, album);
                    }
                }

                field = json_object_get_string( entry_object, "artist" );
                if ( field != NULL )
                {
                    if ( (artistList == NULL) ||
                       (g_slist_find_custom( artistList, field, artistExists) == NULL ) )
                    {
                        artist = json_serialize_to_string(entry_value);
                        artistList = g_slist_append(artistList, artist);
                    }
                }
            }
            else
            {
                piboxLogger(LOG_ERROR, "Failed fopen on %s: st_size = 0\n", filename);
            }
        }
        closedir (dir);
    } 
    else
    {
        /* could not open directory */
        piboxLogger(LOG_INFO, "Failed to open diretory: %s\n", path);
    }

    free(basedir);
}

/*
 *========================================================================
 * Name:   sortByAlbum
 * Prototype:  gint sortByAlbum( gconstpointer a, gconstpointer b )
 *
 * Description:
 * Compare two JSON entries based on album field.
 *========================================================================
 */
gint 
sortByAlbum ( gconstpointer item_a, gconstpointer item_b )
{
    JSON_Value  *music_a_value;
    JSON_Value  *music_b_value;
    JSON_Object *music_a_object;
    JSON_Object *music_b_object;

    const char *album_a;
    const char *album_b;

    int  rc;

    music_a_value  = json_parse_string( (char *)item_a );
    music_b_value  = json_parse_string( (char *)item_b );
    music_a_object = json_value_get_object( music_a_value );
    music_b_object = json_value_get_object( music_b_value );

    album_a = json_object_get_string( music_a_object, "album" );
    album_b = json_object_get_string( music_b_object, "album" );

    if ( (album_a == NULL) && (album_b == NULL) )
        rc = 0;
    else if ( (album_a == NULL) && (album_b != NULL) )
        rc = 1;
    else if ( (album_a != NULL) && (album_b == NULL) )
        rc = -1;
    else
        rc = strcmp(album_a, album_b);

    json_value_free(music_a_value);
    json_value_free(music_b_value);

    return(rc);
}

/*
 *========================================================================
 * Name:   sortByTrack
 * Prototype:  gint sortByTrack( gconstpointer a, gconstpointer b )
 *
 * Description:
 * Compare two JSON entries based on track number field.
 *========================================================================
 */
gint
sortByTrack ( gconstpointer item_a, gconstpointer item_b )
{
    JSON_Value  *music_a_value;
    JSON_Value  *music_b_value;
    JSON_Object *music_a_object;
    JSON_Object *music_b_object;

    const char *album_a;
    const char *album_b;
    char *track_a = NULL;
    char *track_b = NULL;
    char *ptr = NULL;

    int track_a_num;
    int track_b_num;

    int  rc;

    music_a_value  = json_parse_string( (char *)item_a );
    music_b_value  = json_parse_string( (char *)item_b );
    music_a_object = json_value_get_object( music_a_value );
    music_b_object = json_value_get_object( music_b_value );

    album_a = json_object_get_string( music_a_object, "track" );
    album_b = json_object_get_string( music_b_object, "track" );

    if ( (album_a == NULL) && (album_b == NULL) )
        rc = 0;
    else if ( (album_a == NULL) && (album_b != NULL) )
        rc = 1;
    else if ( (album_a != NULL) && (album_b == NULL) )
        rc = -1;
    else
    {
        /* Some tracks are in the form "a/b". We just want the "a" part. */
        track_a = strdup(album_a);
        track_b = strdup(album_b);

        for(ptr=track_a; *ptr != '\0'; ptr++)
        {
            if ( *ptr == '/' )
            {
                *ptr = '\0';
                break;
            }
        }
        for(ptr=track_a; *ptr != '\0'; ptr++)
        {
            if ( *ptr == '/' )
            {
                *ptr = '\0';
                break;
            }
        }

        track_a_num = atoi(track_a);
        track_b_num = atoi(track_b);
        free(track_a);
        free(track_b);

        /* If a > b, this returns positive value, if <b negative and if == then 0 */
        rc = track_a_num-track_b_num;
    }

    json_value_free(music_a_value);
    json_value_free(music_b_value);

    return(rc);
}

/*
 *========================================================================
 * Name:   sortByArtist
 * Prototype:  gint sortByArtist( gconstpointer a, gconstpointer b )
 *
 * Description:
 * Compare two JSON entries based on artist field.
 *========================================================================
 */
gint 
sortByArtist ( gconstpointer item_a, gconstpointer item_b )
{
    JSON_Value  *music_a_value;
    JSON_Value  *music_b_value;
    JSON_Object *music_a_object;
    JSON_Object *music_b_object;

    const char *artist_a;
    const char *artist_b;

    int  rc;

    music_a_value  = json_parse_string( (char *)item_a );
    music_b_value  = json_parse_string( (char *)item_b );
    music_a_object = json_value_get_object( music_a_value );
    music_b_object = json_value_get_object( music_b_value );

    artist_a = json_object_get_string( music_a_object, "artist" );
    artist_b = json_object_get_string( music_b_object, "artist" );

    if ( (artist_a == NULL) && (artist_b == NULL) )
        rc = 0;
    else if ( (artist_a == NULL) && (artist_b != NULL) )
        rc = 1;
    else if ( (artist_a != NULL) && (artist_b == NULL) )
        rc = -1;
    else
        rc = strcmp(artist_a, artist_b);

    json_value_free(music_a_value);
    json_value_free(music_b_value);

    return(rc);
}

/*
 *========================================================================
 * Name:   dbLoad
 * Prototype:  void dbLoad( void )
 *
 * Description:
 * Load the database into a local link list.
 *
 * Notes:
 * We changed to the top of the db directory tree when we started, so 
 * we just search for db's from the current location.
 *========================================================================
 */
void 
dbLoad()
{
    /*
     * Initialize JSON root object.
     */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    music_value = json_value_init_array();
    music_array = json_value_get_array(music_value);
    json_object_set_value(root_object, "music", music_value);

    /*
     * Create list of directories for each type of db.
     */
    getDirs(&dirList);

    /* 
     * Run the list and read in the json files in each directory.
     */
    piboxLogger(LOG_INFO, "Calling readDB on dirList\n");
    g_slist_foreach(dirList, (GFunc)readDB, 0);
    
    // Sort the lists
    albumList = g_slist_sort(albumList, sortByAlbum);
    artistList = g_slist_sort(artistList, sortByArtist);
}

/*
 *========================================================================
 * Name:   clearCustomList
 * Prototype:  void clearCustomList( void )
 *
 * Description:
 * Clears the artistAlbumList.
 *========================================================================
 */
void
clearCustomList ()
{
    g_slist_free( customList );
    customList = NULL;
}

/*
 *========================================================================
 * Name:   genList
 * Prototype:  void genList( char *searchTerm, int type )
 *
 * Description:
 * Generate a custom list based on artist of album.
 *
 * Arguments:
 * char *searchTerm     The term to search for, as in either artist or album name.
 * int type             DB_ALBUM, DB_ARTIST
 *========================================================================
 */
void 
genList(char *searchTerm, int type)
{
    GSList      *iterator;
    const char  *field;
    JSON_Value  *music_value;
    JSON_Object *music_object;

    gint (*searcher)(gconstpointer, gconstpointer);

    if (searchTerm == NULL )
        return;

    clearCustomList();
    for(iterator = allList; iterator != NULL; iterator = iterator->next)
    {
        music_value = json_parse_string( (char *)iterator->data );
        music_object = json_value_get_object( music_value );

        if ( type == DB_ARTIST )
        {
            piboxLogger(LOG_INFO, "Looking for artist\n");
            field = json_object_get_string( music_object, "artist" );
            searcher = albumExists;
        }
        else
        {
            piboxLogger(LOG_INFO, "Looking for album\n");
            field = json_object_get_string( music_object, "album" );
            searcher = trackExists;
        }
        if ( field == NULL )
        {
            json_value_free(music_value);
            continue;
        }

        piboxLogger(LOG_INFO, "Search term: %s -- Field: %s\n", searchTerm, field);
        if ( strcmp(searchTerm, field) == 0 )
        {
            /* Now we need to see if this entry is already in the list. */
            if ( type == DB_ARTIST )
                field = json_object_get_string( music_object, "album" );
            else
                field = json_object_get_string( music_object, "title" );
            if ( ( customList == NULL ) ||
                 ( g_slist_find_custom(customList, field, searcher) == NULL ) )
            {
                piboxLogger(LOG_INFO, "Adding field to customList\n");
                customList = g_slist_append(customList, (char *)iterator->data);
            }
        }
        json_value_free(music_value);
    }

    /* We want the list sorted by track if necessary. */
    if ( type == DB_ALBUM )
    {
        customList = g_slist_sort(customList, sortByTrack);
    }
}

/*
 *========================================================================
 * Name:   getListSize
 * Prototype:  gint getListSize( gint idx )
 *
 * Description:
 * Get the size of a list.
 *
 * Arguments:
 * int idx          DB_ALBUM, DB_ARTIST, DB_ALL
 *========================================================================
 */
gint 
getListSize (gint idx)
{

    if ( idx == DB_ALBUM )
    {
        return g_slist_length( albumList );
    }
    else if ( idx == DB_ARTIST )
    {
        return g_slist_length( artistList );
    }
    else
    {
        return g_slist_length( customList );
    }
}

/*
 *========================================================================
 * Name:   getTrackTitle
 * Prototype:  const char *getTrackTitle( gint idx )
 *
 * Description:
 * Retrieve an album track field for the indexed entry in the custom list.
 *
 * Arguments:
 * gint  idx     Index into the custom list.
 *
 * Returns:
 * The char * value for the specified field or NULL if the index is
 * invalid.  Caller must free the returned value.
 *========================================================================
 */
const char * 
getTrackTitle (gint idx)
{
    char        *entry = NULL;
    const char  *field;
    JSON_Value  *music_value;
    JSON_Object *music_object;

    entry = g_slist_nth_data( customList, idx );
    if ( entry == NULL )
        return NULL;

    music_value = json_parse_string( entry );
    music_object = json_value_get_object( music_value );
    field = json_object_get_string( music_object, "title" );
    // json_value_free(music_value);
    return (field);
}

/*
 *========================================================================
 * Name:   getTrackNum
 * Prototype:  int getTrackNum( gint idx )
 *
 * Description:
 * Retrieve the album track number for the indexed entry in the custom list.
 *
 * Arguments:
 * gint  idx     Index into the custom list.
 *
 * Returns:
 * The track number or -1 if no track number can be found.
 *========================================================================
 */
int
getTrackNum (gint idx)
{
    char        *entry = NULL;
    const char  *field;
    JSON_Value  *music_value;
    JSON_Object *music_object;

    entry = g_slist_nth_data( customList, idx );
    if ( entry == NULL )
        return(-1);

    music_value = json_parse_string( entry );
    music_object = json_value_get_object( music_value );
    field = json_object_get_string( music_object, "track" );
    if ( field == NULL )
    {
        // json_value_free(music_value);
        return(-1);
    }
    // json_value_free(music_value);
    return (atoi((char *)field));
}

/*
 *========================================================================
 * Name:   getPic
 * Prototype:  char *getPic( gint db, gint idx )
 *
 * Description:
 * Retrieve the image associated with the db entry.
 *
 * Arguments:
 * int  db          DB_ALBUM=albumList, DB_ARTIST=artistList, DB_ALL=customList
 * int  idx         Offset to retrieve.
 *
 * Returns:
 * Pointer to string holding the path to the image or NULL if there is no image
 * for that index. Caller is responsible for freeing the returned pointer.
 *========================================================================
 */
char * 
getPic (gint db, gint idx)
{
    char        *entry = NULL;

    if ( db == DB_ALBUM )
    {
        entry = g_slist_nth_data( albumList, idx );
    }
    else if ( db == DB_ARTIST )
    {
        entry = g_slist_nth_data( artistList, idx );
    }
    else
    {
        if ( customList != NULL )
        {
            entry = g_slist_nth_data( customList, idx );
        }
    }

    return(entry);
}

/*
 *========================================================================
 * Name:   addToDisplay
 * Prototype:  void addToDisplay( gpointer, gpointer )
 *
 * Description:
 * Add an entry from the db list to the display.
 *========================================================================
 */
void 
addToDisplay (gpointer item, gpointer user_data)
{
    GtkListStore    *list  = (GtkListStore *)user_data;
    GtkTreeIter     iter;
    MUSIC_T         *music;

    music = (MUSIC_T *)item;
    if ( music->valid == DB_VALID )
    {
        // Acquire an iterator
        gtk_list_store_append (list, &iter);

        // Save the data in the model
        gtk_list_store_set(list, &iter, 0, music->title, -1);
    }
}

/*
 *========================================================================
 * Name:   findMusicByName
 * Prototype:  gint findMusicByName( gconstpointer, gconstpointer, )
 *
 * Description:
 * Find the music entry based on the display name.
 *========================================================================
 */
gint
findMusicByName( gconstpointer item, gconstpointer user_data )
{
    MUSIC_T     *music;
    char        *name = (char *)user_data;

    if ( name == NULL )
    {   
        fprintf(stderr, "missing music name to search for\n");
        return 0;
    }

    music = (MUSIC_T *)item;
    if ( strcmp(music->title, name) == 0 )
        return 0;
    return 1;
}

/*
 *========================================================================
 * Name:   playMusic
 * Prototype:  void playMusic( int idx )
 *
 * Description:
 * Play the currently active music.
 *========================================================================
 */
void
playMusic ( int idx )
{
    char        *entry;
    const char  *file;
    const char  *basedir;
    JSON_Value  *music_value;
    JSON_Object *music_object;

    entry = g_slist_nth_data( customList, idx );
    if ( entry == NULL )
        return;

    music_value = json_parse_string( entry );
    music_object = json_value_get_object( music_value );
    file = json_object_get_string( music_object, "file" );
    basedir = json_object_get_string( music_object, "basedir" );
    piboxLogger(LOG_INFO, "Music path: %s/%s\n", basedir, file);

    // Cleanup from any previous runs
    shutdownPlayerProcessor();

    // Start new music
    startPlayerProcessor(basedir, file);
    // json_value_free(music_value);
}

/*
 *========================================================================
 * Name:   stopMusic
 * Prototype:  void stopMusic( void )
 *
 * Description:
 * Stop the currently playing music.
 *========================================================================
 */
void
stopMusic()
{
    // Cleanup from any previous runs
    shutdownPlayerProcessor();
}

/*
 *========================================================================
 * Name:   dbDump
 * Prototype:  void dbDump( void )
 *
 * Description:
 * Dump the databases to file.
 *========================================================================
 */
void
dbDump ()
{
    int         i, size;
    char        *entry;
    const char  *album, *artist;
    JSON_Value  *entry_value;
    JSON_Object *entry_object;

    printf("--------------------\n");
    printf("Albums: \n");
    size = getListSize(DB_ALBUM);
    for(i=0; i<size; i++)
    {
        entry = g_slist_nth_data( albumList, i );
        entry_value = json_parse_string( entry );
        entry_object = json_value_get_object( entry_value );
        album = json_object_get_string( entry_object, "album" );
        if ( album != NULL )
            printf("%s\n", album);
        json_value_free(entry_value);
    }

    printf("--------------------\n");
    printf("Artists: \n");

    size = getListSize(DB_ARTIST);
    printf("size: %d\n", size);
    for(i=0; i<size; i++)
    {
        entry = g_slist_nth_data( artistList, i );
        entry_value = json_parse_string( entry );
        entry_object = json_value_get_object( entry_value );
        artist = json_object_get_string( entry_object, "artist" );
        if ( artist != NULL )
            printf("%s\n", artist);
        json_value_free(entry_value);
    }
}

