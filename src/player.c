/*******************************************************************************
 * musicfe
 *
 * player.c:  Start and stop music.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PLAYER_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <pibox/touchProcessor.h>

static int playerIsRunning = 0;
static pthread_mutex_t playerProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t playerProcessorThread;
static pid_t musicPid = -1;

/* FIFO used to send commands to omxplayer. */
static int fifo_fd = -1;

/* Path info for music to be played. */
const char *root = NULL;
const char *musicfile = NULL;

#include "musicfe.h"

/*========================================================================
 * Name:   msgPlayer
 * Prototype:  void msgPlayer( int region )
 *
 * Description:
 * Send a message to the OMX player based on the region provided.  
 *
 *   -------------------------------------------
 *   | 0: Vol Up   | 1: Next      | 2: Stop    | 
 *   -------------------------------------------
 *   | 3: Rewind   | 4:Play/Pause | 5: FF      |
 *   -------------------------------------------
 *   | 6: Vol Down | 7: Prev      | 8: Unused  |
 *   -------------------------------------------
 *
 * Arguments:
 * int region       0-8 for control, 100 is init message required at start time.
 *
 * Notes:
 * This is only valid if you're using the omxplayer!
 *========================================================================*/
void
msgPlayer( int region )
{
    char    buf[4];
    char    left_arrow[3] = {0x1b, 0x5b, 0x43};
    char    right_arrow[3] = {0x1b, 0x5b, 0x44};

    /*
     * Ignore if not using the OMX player.
     */
    if ( !isCLIFlagSet(CLI_OMX) )
    {
        piboxLogger(LOG_ERROR, "Not using omxplayer.  Ignoring region message.\n");
        return;
    }
    piboxLogger(LOG_INFO, "omxplayer in use: region = %d\n", region);

    fifo_fd = open(OMX_FIFO, O_WRONLY | O_NONBLOCK);
    if ( fifo_fd == -1 )
    {
        piboxLogger(LOG_ERROR, "Failed to open the OMX fifo: %s\n", strerror(errno));
        return;
    }
    else
        piboxLogger(LOG_INFO, "Opened FIFO to omxplayer\n");

    piboxLogger(LOG_INFO, "Selecting action.\n");
    switch( region )
    {
        /* Volume up */
        case 0:
            piboxLogger(LOG_INFO, "Sending volume up\n");
            sprintf(buf, "+");
            write(fifo_fd, buf, 1);
            break;

        /* Next music */
        case 1:
            piboxLogger(LOG_INFO, "Setting up next music\n");
            sprintf(buf, "q");
            write(fifo_fd, buf, 1);
            break;

        /* Stop playback */
        case 2:
            piboxLogger(LOG_INFO, "Sending stop playback\n");
            sprintf(buf, "q");
            write(fifo_fd, buf, 1);
            break;

        /* Rewind */
        case 3:
            piboxLogger(LOG_INFO, "Rewind\n");
            // sprintf(buf, "<");
            // write(fifo_fd, buf, 1);
            write(fifo_fd, right_arrow, 3);
            usleep(250000);
            sprintf(buf, "p");
            write(fifo_fd, buf, 1);
            usleep(250000);
            write(fifo_fd, buf, 1);
            break;

        /* Play/Pause */
        case 4:
            piboxLogger(LOG_INFO, "Sending play/pause\n");
            sprintf(buf, "p");
            write(fifo_fd, buf, 1);
            break;

        /* Fast forward */
        case 5:
            piboxLogger(LOG_INFO, "FF\n");
            // sprintf(buf, ">");
            // write(fifo_fd, buf, 1);
            write(fifo_fd, left_arrow, 3);
            usleep(250000);
            sprintf(buf, "p");
            write(fifo_fd, buf, 1);
            usleep(250000);
            write(fifo_fd, buf, 1);
            break;

        /* Volume down */
        case 6:
            piboxLogger(LOG_INFO, "Sending volume down\n");
            sprintf(buf, "+");
            write(fifo_fd, buf, 1);
            break;

        /* Prev music */
        case 7:
            piboxLogger(LOG_INFO, "Setting up prev music\n");
            sprintf(buf, "q");
            write(fifo_fd, buf, 1);
            break;

        /* Unused */
        case 8:
            break;

        /* 
         * Initialization message is special
         * See http://apsvr.com/blog/?p=122
         */
        case 100:
            piboxLogger(LOG_INFO, "Sending initialization\n");
            sprintf(buf, ".\n");
            write(fifo_fd, buf, 2);
            break;

        /* WTF? */
        default:
            piboxLogger(LOG_ERROR, "Invalid region: %d\n", region);
            break;

    }
    close(fifo_fd);
    fifo_fd = -1;
}

/*========================================================================
 * Name:   spawnPlayer
 * Prototype:  int spawnPlayer( const char *filename )
 *
 * Description:
 * Start playing music from the named file.
 *
 * Returns:
 * The process id of the music player or -1 if the player cannot be started.
 *========================================================================*/
static pid_t
spawnPlayer( void )
{
    struct stat stat_buf;
    pid_t       childPid;
    char        *dup, *suffix;
    char        *cmd, *str;
    char        fullpath[MAXBUF];
    char        *args[64];
    char        *arg;
    char        *ptr;
    int         filepos;
    int         found;
    int         i;
    char        cwd[PATH_MAX];

    /*
     * Arguments to launch omxplayer, which has to be run from an xterm
     * xterm -display :0.0 -fullscreen -fg black -bg black -e omxplayer -o hdmi -r <file>
     */
    char omx[MAXBUF];

    /* Safety check */
    if ( root == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing basedir, can't spawn music.\n");
        return -1;
    }
    if ( musicfile == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing filename, can't spawn music.\n");
        return -1;
    }

    /* Fill in full path buffer.  We move up a dir (..) because the root is the database directory. */
    sprintf(fullpath, "%s/../%s", root, musicfile);

    if ( stat(fullpath, &stat_buf) != 0 )
    {
        memset(cwd, 0, 512);
        getcwd(cwd, 512);
        piboxLogger(LOG_ERROR, "No such file: %s (current dir: %s)\n", fullpath, cwd);
        return -1;
    }
    piboxLogger(LOG_INFO, "Found file: %s\n", fullpath);

    /* Get the filename suffix */
    dup = g_strdup( musicfile );
    suffix = strrchr( dup, '.' );
    if ( suffix == NULL )
    {
        piboxLogger(LOG_INFO, "Can't find suffix for file: %s\n", musicfile);
        return -1;
    }

    /* Find the appropiate player command */
    piboxLogger(LOG_INFO, "Looking for player for format: %s\n", suffix);
    cmd = findPlayer((char *)(suffix+1));
    g_free(dup);
    if ( cmd == NULL )
    {
        piboxLogger(LOG_INFO, "Can't find player for format: %s\n", suffix);
        return -1;
    }
    piboxLogger(LOG_INFO, "Found player for format: %s\n", cmd);

    /* Test if this is the omxplayer.  It has special support. */
    if ( strstr(cmd, "omxplayer") != NULL )
    {
        setCLIFlag(CLI_OMX);
    }

    /* 
     * Are we using a touch screen?
     * If so, create the fifo for it.
     */
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        if ( mkfifo(OMX_FIFO, 0666) != 0 )
        {
            piboxLogger(LOG_ERROR, "Failed to create FIFO: %s\n", strerror(errno));
            return(-1);
        }
        else
            piboxLogger(LOG_INFO, "Created FIFO: %s\n", OMX_FIFO);
    }

    /* Create command line that will run the process. */
    piboxLogger(LOG_INFO, "Starting music: %s, path = %s\n", cmd, fullpath);
    childPid = fork();
    if ( childPid == 0 )
    {   
        str = g_strdup(cmd);
        filepos = parse(str, args, 64);
        piboxLogger(LOG_INFO, "filepos = %d\n", filepos);

        /*
         * If this is the omxplayer, then append the FIFO argument.
         */
        if ( isCLIFlagSet(CLI_OMX) && isCLIFlagSet(CLI_TOUCH) )
        {
            /* Rebuild the command to run as a single string */
            memset(omx, 0, MAXBUF);
            for(i=0; args[i]!=NULL; i++)
            {
                strcat(omx, args[i]);
                strcat(omx, " ");
            }

            strcat(omx, "<");
            strcat(omx, OMX_FIFO);

            /* 
             * Parse the command.
             */
            str = g_strdup(omx);
            filepos = parse(str, args, 64);
        }

        /*
         * Replace %s with path to music.
         */
        found = 0;
        for(i=0; i<filepos; i++)
        {
            piboxLogger(LOG_TRACE1, "args[%d] = %s\n", i, args[i]);
            if ( strstr(args[i], "%s") != NULL )
            {
                arg = g_malloc0(strlen(args[i])+strlen(fullpath)+2);
                sprintf(arg, "%s", args[i]);
                ptr = arg;
                while (*ptr != '%' )
                    ptr++;
                memcpy(ptr, fullpath, strlen(fullpath));
                args[i] = arg;
                found = 1;
                break;
            }
        }

        /* Dump the args we exec */
        for(i=0; args[i]!=NULL; i++)
        {
            piboxLogger(LOG_INFO, "Argument[%d]: %s\n", i, args[i]);
        }

        if ( found )
        {
            execvp(args[0], args);
            piboxLogger(LOG_ERROR, "Failed to spawn music player cmd %s, path = %s: %s\n", 
                    cmd, fullpath, strerror(errno));
        }
        else
        {
            piboxLogger(LOG_ERROR, "Missing \\%s argument in player cmd: %s\n", cmd);
        }
        abort();
    }

    /* Wait a couple of seconds and then send an init message to the player */
    if ( isCLIFlagSet(CLI_OMX) && isCLIFlagSet(CLI_TOUCH) )
    {
        sleep(2);
        msgPlayer(100);
    }

    return childPid;
}

/*========================================================================
 * Name:   killPlayer
 * Prototype:  int killPlayer( pid_t pid)
 *
 * Description:
 * Kill a music player based on the process ID and reap the child process.
 *========================================================================*/
void
killPlayer( pid_t pid )
{
    int status;
    kill(pid, SIGTERM);
    pid = waitpid( pid, &status, 0);
    unsetCLIFlag(CLI_OMX);
}


/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isPlayerProcessorRunning
 * Prototype:  int isPlayerProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of playerIsRunning variable.
 *========================================================================*/
static int
isPlayerProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &playerProcessorMutex );
    status = playerIsRunning;
    pthread_mutex_unlock( &playerProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setPlayerProcessorRunning
 * Prototype:  int setPlayerProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of playerIsRunning variable.
 *========================================================================*/
static void
setPlayerProcessorRunning( int val )
{
    pthread_mutex_lock( &playerProcessorMutex );
    playerIsRunning = val;
    pthread_mutex_unlock( &playerProcessorMutex );
}

/*========================================================================
 * Name:   playerProcessor
 * Prototype:  void playerProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from player and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * This loop runs every 100ms but only runs the queue every 3 seconds.
 * This allows the thread to exit quickly when the daemon is shutting down
 * while giving remote users plenty of time to send heartbeats for their streams.
 *========================================================================*/
static void *
playerProcessor( void *arg )
{
    int         status = -1;

    /* Register a callback for touch events. */
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        piboxLogger(LOG_INFO, "Registering msgPlayer.\n");
        piboxTouchRegisterCB(msgPlayer, TOUCH_REGION);
    }

    /* Spawn the music player */
    musicPid = spawnPlayer();
    if ( musicPid == -1 )
        return(0);
    setPlayerProcessorRunning(1);

    /* Wait to reap child */
    musicPid = waitpid( musicPid, &status, 0);
    musicPid = -1;

    /* Reregister musicfe handler, if necessary */
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        piboxLogger(LOG_INFO, "Registering musicTouch.\n");
        piboxTouchRegisterCB(musicTouch, TOUCH_REGION);
    }

    setPlayerProcessorRunning(0);

    piboxLogger(LOG_INFO, "Player thread is exiting.\n");
    return(0);
}

/*========================================================================
 * Name:   startPlayerProcessor
 * Prototype:  void startPlayerProcessor( const char *basedir, const char *filename )
 *
 * Description:
 * Setup thread to play music file.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownPlayerProcessor().
 *========================================================================*/
void
startPlayerProcessor( const char *basedir, const char *filename )
{
    int rc;

    /* Prevent running two at once. */
    if ( isPlayerProcessorRunning() )
        return;

    /* Save the configuration. */
    root = basedir;
    musicfile = filename;

    /* Create a thread to expire streams. */
    rc = pthread_create(&playerProcessorThread, NULL, &playerProcessor, NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create player thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started player thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownPlayerProcessor
 * Prototype:  void shutdownPlayerProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownPlayerProcessor( void )
{
    int timeOut = 0;

    // Try to kill any running music.
    if ( musicPid != -1 )
        killPlayer(musicPid);

    if ( isPlayerProcessorRunning() )
    {
        while ( isPlayerProcessorRunning() )
        {
            sleep(1);
            timeOut++;
            if (timeOut == 60)
            {
                piboxLogger(LOG_ERROR, "Timed out waiting on player thread to shut down.\n");
                return;
            }
        }
        pthread_detach(playerProcessorThread);
    }

    /* Clean up FIFO, if it exists. */
    unlink(OMX_FIFO);

    piboxLogger(LOG_INFO, "playerProcessor shut down.\n");
}

